function createProject(name) {
  this.name = name;
  this.list = [];
}

function createTodo(name, dueDate, priority, description, status) {
  this.name = name;
  this.dueDate = dueDate;
  this.priority = priority;
  this.description = description;
  this.status = status;
}

function addProject() {
  let project = document.getElementById("projectName").value;
  projectObj = new createProject(project);
  displayProjectList(projectObj);
}

function displayProjectList(project) {
  var projectList = document.createElement("li");
  projectList.className = "list-group-item";
  projectList.style.backgroundColor = "#7a7a8c";
  var viewBtn = document.createElement("button");
  var projectName = document.createTextNode(project.name);

  projectList.appendChild(projectName);

  viewBtn.className = "btn btn-primary float-right edit";
  viewBtn.appendChild(document.createTextNode("View"));
  viewBtn.addEventListener("click", showTodo.bind(null, project));

  projectList.appendChild(viewBtn);

  var deleteBtn = document.createElement("button");
  deleteBtn.className = "btn btn-danger btn-xs float-right delete";
  deleteBtn.appendChild(document.createTextNode("delete"));
  deleteBtn.addEventListener("click", removeProject);

  projectList.appendChild(deleteBtn);

  document.getElementById("projectList").appendChild(projectList);
}

function getTodoData() {
  let listName = document.getElementById("list_name").value;
  let dueDate = document.getElementById("duedate").value;
  let priority = document.getElementById("priority").value;
  let description = document.getElementById("description").value;
  let list = new createTodo(listName, dueDate, priority, description);
  display_todo(list);
}

function display_todo(list) {
  var li = document.createElement("li");
  li.className = "list-group-item";
  li.style.backgroundColor = "#7a7a8c";
  var editBtn = document.createElement("button");
  var textNode = document.createTextNode(
    list.name +
      " " +
      list.dueDate +
      " " +
      list.priority +
      " " +
      list.description
  );
  li.appendChild(textNode);

  editBtn.className = "btn btn-primary btn-sm float-right edit";
  editBtn.appendChild(document.createTextNode("Edit"));
  editBtn.addEventListener("click", editTodo.bind(null, list));
  li.appendChild(editBtn);

  var deleteBtn = document.createElement("button");
  deleteBtn.className = "btn btn-danger btn-sm float-right delete";
  deleteBtn.appendChild(document.createTextNode("delete"));
  deleteBtn.addEventListener("click", removeTodo);
  li.appendChild(deleteBtn);

  document.getElementById("todoList").appendChild(li);
}

function removeTodo(li) {
  var itemList = document.getElementById("todoList");
  if (li.target.classList.contains("delete")) {
    var li = li.target.parentElement;
    itemList.removeChild(li);
  }
}

function removeProject(li) {
  var itemList = document.getElementById("projectList");
  if (li.target.classList.contains("delete")) {
    var li = li.target.parentElement;
    itemList.removeChild(li);
  }
}

function showTodo() {
  location.href = "project_name.html";
}

function editTodo(li) {
  document.getElementById("list_name").value = li.name;
  document.getElementById("duedate").value = li.dueDate;
  document.getElementById("priority").value = li.priority;
  document.getElementById("description").value = li.description;
}
